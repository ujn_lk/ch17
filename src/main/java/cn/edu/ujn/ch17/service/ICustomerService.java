package cn.edu.ujn.ch17.service;

import java.util.List;

import cn.edu.ujn.ch17.dao.Customer;

public interface ICustomerService {
	public Customer findCustomerById(Integer id);
	public int addCustomer(Customer customer);
	public List<Customer> findCustomers();
}
