package cn.edu.ujn.ch17.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.ujn.ch17.dao.Customer;
import cn.edu.ujn.ch17.dao.CustomerMapper;
@Service
public class CustomerServiceImpl implements ICustomerService{

	@Autowired
	private CustomerMapper customerMapper;
	@Override
	public Customer findCustomerById(Integer id) {
		// TODO Auto-generated method stub
		return customerMapper.selectByPrimaryKey(id);
	}
	@Override
	public int addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		return customerMapper.insert(customer);
	}
	@Override
	public List<Customer> findCustomers() {
		// TODO Auto-generated method stub
		return customerMapper.findCustomers();
	}

}
