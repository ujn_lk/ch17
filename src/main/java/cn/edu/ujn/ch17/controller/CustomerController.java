package cn.edu.ujn.ch17.controller;

import java.util.List;

import org.eclipse.jdt.internal.compiler.env.IModule.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.ujn.ch17.dao.Customer;
import cn.edu.ujn.ch17.service.ICustomerService;

@Controller
public class CustomerController {
		@Autowired
		private ICustomerService customerService;
		
		
		@RequestMapping("/customer/{id}")
		public String findById(@PathVariable("id") int id ,Model model) {
			Customer findById = customerService.findCustomerById(id);
			model.addAttribute("customer",findById);
			System.out.println(findById);
			return "customer";
		}
		
		@RequestMapping("/customerdetail/{id}")
		public String findcustomerById(@PathVariable("id") int id ,Model model) {
			Customer customer = customerService.findCustomerById(id);
			model.addAttribute("data",customer);
			System.out.println(customer);
			return "redirect:detailCustomer";
		}
		
		
		@RequestMapping("/addCustomer")
		public String toaddCus() {
			return "addCustomer";
		}
	    @PostMapping("/addCustomer")
	    public String addCustomer(Customer customer) {
	    	System.out.println(customer);
	    	if (customerService.addCustomer(customer)==1) {
				return "success";
			}
	    	return "addCustomer";
	    }
	    
	    @RequestMapping("/listCustomer")
	    public String listCustomer(Model model) {
	    	List<Customer> customers=customerService.findCustomers();
	    	
	    	model.addAttribute("list", customers);
	    	return "listCustomer";
	    }
	    
	    @RequestMapping("/detail")
	    public String detailCustoemr() {
	    	return "detailCustomer";
	    }
}
