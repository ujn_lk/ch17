package cn.edu.ujn.ch17.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import cn.edu.ujn.ch17.model.User;


@Controller
public class UserController {
	
	@GetMapping("/login")
    public String tologin() {
        return "login";
    }

    @PostMapping("/login")
    public String login(User user, HttpServletRequest request) {
        System.out.println("接收到的登录信息:" + user);
        HttpSession session = request.getSession();
        session.setAttribute("USER_SESSION", user);
        return "success";
    }

}
