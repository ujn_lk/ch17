package cn.edu.ujn.ch17.dao;

import java.util.List;

import cn.edu.ujn.ch17.dao.Customer;

public interface CustomerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Customer row);

    int insertSelective(Customer row);

    Customer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Customer row);

    int updateByPrimaryKey(Customer row);
    
    public List<Customer> findCustomers();

}