<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<html>
<head>
<title>客户详细信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
	function search() {
		// 获取输入的查询编号
		var id = $("#number").val();
		$.ajax({
			url : "${pageContext.request.contextPath }/customer/" + id,
			type : "GET",
			//定义回调响应的数据格式为JSON字符串,该属性可以省略
			dataType : "json",
			//成功响应的结果
			success : function(data) {
				if (data.username != null) {
					alert("返回的JSON是：" + JSON.stringify(data));
					$("#id").text(data.id);
					$("#username").text(data.username);
					$("#jobs").text(data.jobs);
					$("#phone").text(data.phone);
				} else {
					alert("没有找到id为:" + id + "的用户！");
				}
			}
		});
	}
</script>
</head>
<body>
	<table  border=1>
		<tr>
			<td colspan="4">
				<form >
					编号：<input type="text" name="number" id="number"> 
					<input type="button" value="查询" onclick="search()" />
						
						<!--<input type="submit" value="查询"/><br>action="${pageContext.request.contextPath }/customer/{id}"  -->
				</form>
			</td>
		</tr>
		<tr>
			<td>编号</td>
			<td>名称</td>
			<td>职业</td>
			<td>电话</td>
		</tr>
		<tr>
			<td id="id"></td>
			<td id="username"></td>
			<td id="jobs"></td>
			<td id="phone"></td>
		</tr>
	</table>
</body>
</html>
