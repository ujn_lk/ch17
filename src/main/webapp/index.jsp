<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<html>
<body>
<h2>Spring、SpringMVC和MyBatis框架整合!</h2>
<h2><a href="addCustomer" target="_blank">1、测试Customer添加</a></h2><br>
<h2><a href="listCustomer" target="_blank">2、测试查询所有Customer</a></h2><br>
<h2><a href="detail" target="_blank">3、测试RESTful方式根据id查询Customer</a></h2><br>
<h2><a href="listCustomer" target="_blank">4、测试拦截器(先访问listCustomer，被拦回，登录后，再访问listCustomer)</a></h2><br>
</body>
</html>
